<?php

/*
Plugin Name: Pages and Posts
Plugin URI: http://rrankawat.com
Description: This plugin displays Hello World at the end of every page and post.
Version: 1.0
Author: Rakesh Rankawat
Author URI: http://rrankawat.com
*/

// Filter hook
add_filter('the_content', 'display_hello_world');

function display_hello_world($content) {
	return $content . '<p>Hello World!</p>';
}