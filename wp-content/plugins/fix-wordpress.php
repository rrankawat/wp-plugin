<?php

/*
Plugin Name: Fix WordPress
Plugin URI: http://rrankawat.com
Description: Change the spelling of WordPress to capital P
Version: 1.0
Author: Rakesh Rankawat
Author URI: http://rrankawat.com
*/

// Filter hook
add_filter('the_content', array('FixWordPress', 'fixSpelling'));

class FixWordPress {
	function fixSpelling($content) {
		$content = str_replace('wordpress', 'WordPress', $content);
		return $content;
	}
}