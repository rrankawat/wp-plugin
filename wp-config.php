<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-plugin' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T/?VjZV.F5 kOF;C1&?>#ZW{(S+CW@PFAvhc E:{E&,T~zcf,N82!O%_{SAh=bQ6' );
define( 'SECURE_AUTH_KEY',  'Nu*o5[_QX[+2/Sbn4$>}ioSL:q%hL5<9j#8Q+JpX>].X}fN8]k?`W7SOX|s-wHf}' );
define( 'LOGGED_IN_KEY',    'n~mu0NRz|t+.T/%.a+^J%X0[j#Osr4Zj_e ,~k@^+gMN~EJNm2t+ftF:.=#_E)7j' );
define( 'NONCE_KEY',        '5##9P/kZ<4|E#.XRA/@8v2-DqB&&kwSOvA`/W_ephb8B&Yl}=v+adexMN.o/nE0<' );
define( 'AUTH_SALT',        'vj8uKH,U,28nk6$ FWH%xBG0^Y}ay_U=If4Wo:^[t{nxN o^OZzwE3NkZooM>XE2' );
define( 'SECURE_AUTH_SALT', ']q:_kYzF08]v]*I(@Hf@r)B}(?aV[&!bWhEUvkX9)JO>:eD]Y2oQmhVIj;*>8Mq_' );
define( 'LOGGED_IN_SALT',   'CY;,[d5/Hdp07By|z)ye}0uLqnUc:&ni$F6tD4fbEfK!.g-Bx<Cn[+swy3F;1QkJ' );
define( 'NONCE_SALT',       '&~=rbS/`zhHz({m!az8(( .R@4]KX[Rt?BGzl^X$G91tg{SkX_t(`#GBTwHj@n/`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
